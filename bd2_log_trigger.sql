create database if not exists empresa;

use empresa;

drop table if exists depto_localizacoes;
drop table if exists departamento;
drop table if exists funcionario;
drop table if exists log;

-- **************************** CRIACAO DAS TABELAS ****************************

create table funcionario (
	pnome varchar(20) not null,
	minicial char(1) not null,
	unome varchar(20) not null,
	cpf char(11) not null,
	datanasc date not null,
	endereco varchar(40) not null,
	sexo char(1) not null,
	salario numeric(20,2) not null,
	primary key (cpf)
) ENGINE=InnoDB;

create table departamento (
	dnome varchar(20) not null,
	dnumero int not null,
	gercpf char(11) not null,
	gerdatainicio date not null,
	primary key (dnumero),
	unique(dnome),
	foreign key (gercpf) references funcionario(cpf)
) ENGINE=InnoDB;

create table depto_localizacoes (
	dnumero int not null,
	dlocalizacao varchar(20) not null,
	primary key ( dnumero, dlocalizacao ),
	foreign key(dnumero) references departamento (dnumero)
) ENGINE=InnoDB;

create table log (
	id int AUTO_INCREMENT not null,
	usuario varchar(50) not null,
	transacao varchar(50) not null,
	operacao varchar(20) not null,
	tabela varchar(20) not null,
	data_atualizacao datetime DEFAULT null,
	time_log varchar(10) not null,
	campo1 varchar(20),
	campo2 varchar(20),
	campo3 varchar(40),
	campo4 char(1),
	campo5 char(1),
	campo6 char(11),
	campo7 date,
	campo8 numeric(20,2),
	campo9 int,
	primary key (id)
) ENGINE=InnoDB;
 


-- **************************** CRIACAO DAS TRIGGERS ****************************


DELIMITER $$
	
	-- INSERT TRIGGERS
    CREATE
		TRIGGER `func_after_insert_funcionario` AFTER INSERT 
		ON `funcionario` 
		FOR EACH ROW BEGIN
			INSERT INTO log SET 
			usuario = current_user(),
			transacao = 'connection_id()',
			operacao = 'insert',
			tabela = 'funcionario',
			data_atualizacao = NOW(),
			time_log = 'after',
			campo1 = NEW.pnome,
			campo4 = NEW.minicial,
			campo2 = NEW.unome,
			campo6 = NEW.cpf,
			campo7 = NEW.datanasc,
			campo3 = NEW.endereco,
			campo5 = NEW.sexo,
			campo8 = NEW.salario;
    END$$

    CREATE
		TRIGGER `func_after_insert_departamento` AFTER INSERT 
		ON `departamento` 
		FOR EACH ROW BEGIN
			INSERT INTO log SET 
			usuario = current_user(),
			transacao = 'connection_id()',
			operacao = 'insert',
			tabela = 'departamento',
			data_atualizacao = NOW(),
			time_log = 'after',
			campo1 = NEW.dnome,
			campo9 = NEW.dnumero,
			campo6 = NEW.gercpf,
			campo7 = NEW.gerdatainicio;
    END$$

    CREATE
		TRIGGER `func_after_insert_depto_localizacoes` AFTER INSERT 
		ON `depto_localizacoes` 
		FOR EACH ROW BEGIN
			INSERT INTO log SET 
			usuario = current_user(),
			transacao = 'connection_id()',
			operacao = 'insert',
			tabela = 'dpto_localizacoes',
			data_atualizacao = NOW(),
			time_log = 'after',
			campo9 = new.dnumero,
			campo1 = new.dlocalizacao;
    END$$

    -- UPDATE TRIGGERS

    CREATE
		TRIGGER `func_before_update_funcionario` BEFORE UPDATE 
		ON `funcionario` 
		FOR EACH ROW BEGIN
			INSERT INTO log SET 
			usuario = current_user(),
			transacao = 'connection_id()',
			operacao = 'update',
			tabela = 'funcionario',
			data_atualizacao = NOW(),
			time_log = 'before',
			campo1 = OLD.pnome,
			campo4 = OLD.minicial,
			campo2 = OLD.unome,
			campo6 = OLD.cpf,
			campo7 = OLD.datanasc,
			campo3 = OLD.endereco,
			campo5 = OLD.sexo,
			campo8 = OLD.salario;
    END$$

    CREATE
		TRIGGER `func_after_update_funcionario` AFTER UPDATE 
		ON `funcionario` 
		FOR EACH ROW BEGIN
			INSERT INTO log SET 
			usuario = current_user(),
			transacao = 'connection_id()',
			operacao = 'update',
			tabela = 'funcionario',
			data_atualizacao = NOW(),
			time_log = 'after',
			campo1 = NEW.pnome,
			campo4 = NEW.minicial,
			campo2 = NEW.unome,
			campo6 = NEW.cpf,
			campo7 = NEW.datanasc,
			campo3 = NEW.endereco,
			campo5 = NEW.sexo,
			campo8 = NEW.salario;
    END$$

    CREATE
		TRIGGER `func_before_update_departamento` BEFORE UPDATE 
		ON `departamento` 
		FOR EACH ROW BEGIN

			INSERT INTO log SET 
			usuario = current_user(),
			transacao = 'connection_id()',
			operacao = 'update',
			tabela = 'departamento',
			data_atualizacao = NOW(),
			time_log = 'before',
			campo1 = OLD.dnome,
			campo9 = OLD.dnumero,
			campo6 = OLD.gercpf,
			campo7 = OLD.gerdatainicio;
    END$$

    CREATE
		TRIGGER `func_after_update_departamento` AFTER UPDATE 
		ON `departamento` 
		FOR EACH ROW BEGIN

			INSERT INTO log SET 
			usuario = current_user(),
			transacao = 'connection_id()',
			operacao = 'update',
			tabela = 'departamento',
			data_atualizacao = NOW(),
			time_log = 'after',
			campo1 = NEW.dnome,
			campo9 = NEW.dnumero,
			campo6 = NEW.gercpf,
			campo7 = NEW.gerdatainicio;
    END$$

    CREATE
		TRIGGER `func_before_update_depto_localizacoes` BEFORE UPDATE 
		ON `depto_localizacoes` 
		FOR EACH ROW BEGIN
			INSERT INTO log SET 
			usuario = current_user(),
			transacao = 'connection_id()',
			operacao = 'update',
			tabela = 'dpto_localizacoes',
			data_atualizacao = NOW(),
			time_log = 'before',
			campo9 = OLD.dnumero,
			campo1 = OLD.dlocalizacao;
    END$$

    CREATE
		TRIGGER `func_after_update_depto_localizacoes` AFTER UPDATE 
		ON `depto_localizacoes` 
		FOR EACH ROW BEGIN

			INSERT INTO log SET 
			usuario = current_user(),
			transacao = 'connection_id()',
			operacao = 'update',
			tabela = 'dpto_localizacoes',
			data_atualizacao = NOW(),
			time_log = 'after',
			campo9 = new.dnumero,
			campo1 = new.dlocalizacao;
    END$$



    -- TRIGGERS DELETE

    CREATE
		TRIGGER `func_before_delete_funcionario` BEFORE DELETE 
		ON `funcionario` 
		FOR EACH ROW BEGIN

			INSERT INTO log SET 
			usuario = current_user(),
			transacao = 'connection_id()',
			operacao = 'delete',
			tabela = 'funcionario',
			data_atualizacao = NOW(),
			time_log = 'before',
			campo1 = OLD.pnome,
			campo4 = OLD.minicial,
			campo2 = OLD.unome,
			campo6 = OLD.cpf,
			campo7 = OLD.datanasc,
			campo3 = OLD.endereco,
			campo5 = OLD.sexo,
			campo8 = OLD.salario;
    END$$

    CREATE
		TRIGGER `func_before_delete_departamento` BEFORE DELETE 
		ON `departamento` 
		FOR EACH ROW BEGIN
			INSERT INTO log SET 
			usuario = current_user(),
			transacao = 'connection_id()',
			operacao = 'delete',
			tabela = 'departamento',
			data_atualizacao = NOW(),
			time_log = 'before',
			campo1 = OLD.dnome,
			campo9 = OLD.dnumero,
			campo6 = OLD.gercpf,
			campo7 = OLD.gerdatainicio;
    END$$

    CREATE
		TRIGGER `func_before_delete_depto_localizacoes` BEFORE DELETE 
		ON `depto_localizacoes` 
		FOR EACH ROW BEGIN

			INSERT INTO log SET 
			usuario = current_user(),
			transacao = 'connection_id()',
			operacao = 'delete',
			tabela = 'depto_localizacoes',
			data_atualizacao = NOW(),
			time_log = 'before',
			campo9 = OLD.dnumero,
			campo1 = OLD.dlocalizacao;
    END$$

DELIMITER ;


-- **************************** CASOS DE TESTE ****************************


INSERT INTO `funcionario` (`pnome`, `minicial`, `unome`, `cpf`, `datanasc`, `endereco`, `sexo`, `salario`) VALUES ('Eliseu', 'G', 'Silva', '03651755134', '2016-02-01', 'UFG', 'P', '25000');
INSERT INTO `departamento` (`dnome`, `dnumero`, `gercpf`, `gerdatainicio`) VALUES ('RH', '24', '03651755134', '2011-02-01');
INSERT INTO `depto_localizacoes` (`dnumero`, `dlocalizacao`) VALUES ('24', 'Itu');

UPDATE funcionario SET pnome = 'Abraham Lincoln', minicial = 'L', unome = 'Lopes', cpf = '03651755134', datanasc = '2010-01-01', endereco = 'Sudoeste', sexo = 'M', salario = '20000' WHERE cpf = '03651755134';
UPDATE departamento SET dnome = 'TI', dnumero ='24', gercpf ='03651755134', gerdatainicio ='2013-09-09' WHERE dnumero = '24';
UPDATE depto_localizacoes SET dnumero = '24', dlocalizacao = 'Manaus' WHERE dnumero = '24';

DELETE FROM depto_localizacoes WHERE dnumero='24';
DELETE FROM departamento WHERE dnumero='24';
DELETE FROM funcionario WHERE cpf='03651755134';


-- **************************** REMOCAO DE TRIGGERS ****************************


DROP TRIGGER func_after_insert_funcionario;
DROP TRIGGER func_after_insert_departamento;
DROP TRIGGER func_after_insert_depto_localizacoes;
DROP TRIGGER func_before_update_funcionario;
DROP TRIGGER func_after_update_funcionario;
DROP TRIGGER func_before_update_departamento;
DROP TRIGGER func_after_update_departamento;
DROP TRIGGER func_before_update_depto_localizacoes;
DROP TRIGGER func_after_update_depto_localizacoes;
DROP TRIGGER func_before_delete_funcionario;
DROP TRIGGER func_before_delete_departamento;
DROP TRIGGER func_before_delete_depto_localizacoes;



-- **************************** SCRIPT DE RECUPERACAO ****************************

DELIMITER $$
  CREATE PROCEDURE recuperar_registro(IN id_log int, OUT id_var int, OUT usuario_var varchar(50), OUT transacao_var varchar(50), OUT operacao_var varchar(50), OUT tabela_var varchar(20), OUT data_atualizacao_var datetime, OUT time_log_var varchar(10), OUT campo1_var varchar(20), OUT campo2_var varchar(20), OUT campo3_var varchar(40), OUT campo4_var char(1), OUT campo5_var char(1), OUT campo6_var char(11), OUT campo7_var date, OUT campo8_var decimal(20,2), OUT campo9_var int)
  BEGIN 
    SELECT
    id, usuario, transacao, operacao, tabela, data_atualizacao, time_log, campo1, campo2, campo3, campo4, campo5, campo6, campo7, campo8, campo9
    INTO id_var, usuario_var, transacao_var, operacao_var, tabela_var, data_atualizacao_var, time_log_var, campo1_var, campo2_var, campo3_var, campo4_var, campo5_var, campo6_var, campo7_var, campo8_var, campo9_var
    FROM log
    WHERE id=id_log;
  END $$

DELIMITER ;



DELIMITER $$
  CREATE PROCEDURE restaurar_registro_trancacao(OUT result varchar(50), IN usuario_var varchar(50), IN transacao_var varchar(50), IN operacao_var varchar(50), IN tabela_var varchar(20), IN data_atualizacao_var datetime, IN time_log_var varchar(10), IN campo1_var varchar(20), IN campo2_var varchar(20), IN campo3_var varchar(40), IN campo4_var char(1), IN campo5_var char(1), IN campo6_var char(11), IN campo7_var date, IN campo8_var decimal(20,2), IN campo9_var int)
  BEGIN
      IF (operacao_var = 'delete') THEN
          IF (tabela_var = 'funcionario') THEN
              INSERT INTO funcionario VALUES (campo1_var, campo4_var, campo2_var, campo6_var, campo7_var, campo3_var, campo5_var, campo8_var);
              SET result = "funcionario - insert";
          ELSE 
              IF (tabela_var = 'departamento') THEN
                  INSERT INTO departamento VALUES (campo1_var, campo9_var, campo6_var, campo7_var);
                  SET result = "departamento - insert";
              ELSE
                  INSERT INTO depto_localizacoes VALUES (campo9_var, campo1_var);
                  SET result = "depto-localizacoes - insert";
              END IF;
          END IF;

      ELSE 
          IF (operacao_var = 'update') THEN
              IF (tabela_var = 'funcionario') THEN
                  UPDATE funcionario
                  SET pnome = campo1_var, minicial = campo4_var, unome = campo2_var, cpf = campo6_var, datanasc = campo7_var, endereco = campo3_var, sexo = campo5_var, salario = campo8_var
                  WHERE cpf = campo6_var;
                  SET result = "funcionario - update";
              ELSE 
                  IF (tabela_var = 'departamento') THEN
                      UPDATE departamento
                      SET pnome = dnome = campo1_var, dnumero = campo9_var, gercpf = campo6_var, gerdatainicio = campo7_var
                      WHERE dnumero = campo9_var;
                      SET result = "departamento - update";
                  ELSE
                      UPDATE depto_localizacoes
                      SET dnumero = campo9_var, dlocalizacao = campo1_var
                      WHERE dnumero = campo9_var;
                      SET result = "depto-localizacoes - update";
                  END IF;
              END IF;
          ELSE
              IF (tabela_var = 'funcionario') THEN
                  DELETE FROM funcionario WHERE cpf=campo6_var;
                  SET result = "funcionario - delete";
              ELSE 
                  IF (tabela_var = 'departamento') THEN
                      DELETE FROM departamento WHERE dnumero=campo9_var;
                      SET result = "departamento - delete";
                  ELSE
                      DELETE FROM depto_localizacoes WHERE dnumero=campo9_var;
                      SET result = "depto-localizacoes - delete";
                  END IF;
              END IF;
          END IF;
      END IF;
  END $$
DELIMITER ;


-- **************************** TESTES DE RECUPERACAO ****************************

CALL recuperar_registro(13, @id, @usuario, @transacao, @operacao, @tabela, @data_atualizacao, @time_log, @campo1, @campo2, @campo3, @campo4, @campo5, @campo6, @campo7, @campo8, @campo9);
CALL restaurar_registro_trancacao(@result, @usuario, @transacao, @operacao, @tabela, @data_atualizacao, @time_log, @campo1, @campo2, @campo3, @campo4, @campo5, @campo6, @campo7, @campo8, @campo9);
SELECT @result;