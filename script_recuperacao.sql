DELIMITER $$
  CREATE PROCEDURE recuperar_registro(IN id_log int, OUT id_var int, OUT usuario_var varchar(50), OUT transacao_var varchar(50), OUT operacao_var varchar(50), OUT tabela_var varchar(20), OUT data_atualizacao_var datetime, OUT time_log_var varchar(10), OUT campo1_var varchar(20), OUT campo2_var varchar(20), OUT campo3_var varchar(40), OUT campo4_var char(1), OUT campo5_var char(1), OUT campo6_var char(11), OUT campo7_var date, OUT campo8_var decimal(20,2), OUT campo9_var int)
  BEGIN 
    SELECT
    id, usuario, transacao, operacao, tabela, data_atualizacao, time_log, campo1, campo2, campo3, campo4, campo5, campo6, campo7, campo8, campo9
    INTO id_var, usuario_var, transacao_var, operacao_var, tabela_var, data_atualizacao_var, time_log_var, campo1_var, campo2_var, campo3_var, campo4_var, campo5_var, campo6_var, campo7_var, campo8_var, campo9_var
    FROM log
    WHERE id=id_log;
  END $$

DELIMITER ;


CALL recuperar_registro(1, @id, @usuario, @transacao, @operacao, @tabela, @data_atualizacao, @time_log, @campo1, @campo2, @campo3, @campo4, @campo5, @campo6, @campo7, @campo8, @campo9);
CALL restaurar_registro_trancacao(@result, @usuario, @transacao, @operacao, @tabela, @data_atualizacao, @time_log, @campo1, @campo2, @campo3, @campo4, @campo5, @campo6, @campo7, @campo8, @campo9);
SELECT @result;


DELIMITER $$
  CREATE PROCEDURE restaurar_registro_trancacao(OUT result varchar(50), IN usuario_var varchar(50), IN transacao_var varchar(50), IN operacao_var varchar(50), IN tabela_var varchar(20), IN data_atualizacao_var datetime, IN time_log_var varchar(10), IN campo1_var varchar(20), IN campo2_var varchar(20), IN campo3_var varchar(40), IN campo4_var char(1), IN campo5_var char(1), IN campo6_var char(11), IN campo7_var date, IN campo8_var decimal(20,2), IN campo9_var int)
  BEGIN
      IF (operacao_var = 'delete') THEN
          IF (tabela_var = 'funcionario') THEN
              INSERT INTO funcionario VALUES (campo1_var, campo4_var, campo2_var, campo6_var, campo7_var, campo3_var, campo5_var, campo8_var);
              SET result = "funcionario - insert";
          ELSE 
              IF (tabela_var = 'departamento') THEN
                  INSERT INTO departamento VALUES (campo1_var, campo9_var, campo6_var, campo7_var);
                  SET result = "departamento - insert";
              ELSE
                  INSERT INTO depto_localizacoes VALUES (campo9_var, campo1_var);
                  SET result = "depto-localizacoes - insert";
              END IF;
          END IF;

      ELSE 
          IF (operacao_var = 'update') THEN
              IF (tabela_var = 'funcionario') THEN
                  UPDATE funcionario
                  SET pnome = campo1_var, minicial = campo4_var, unome = campo2_var, cpf = campo6_var, datanasc = campo7_var, endereco = campo3_var, sexo = campo5_var, salario = campo8_var
                  WHERE cpf = campo6_var;
                  SET result = "funcionario - update";
              ELSE 
                  IF (tabela_var = 'departamento') THEN
                      UPDATE departamento
                      SET pnome = dnome = campo1_var, dnumero = campo9_var, gercpf = campo6_var, gerdatainicio = campo7_var
                      WHERE dnumero = campo9_var;
                      SET result = "departamento - update";
                  ELSE
                      UPDATE depto_localizacoes
                      SET dnumero = campo9_var, dlocalizacao = campo1_var
                      WHERE dnumero = campo9_var;
                      SET result = "depto-localizacoes - update";
                  END IF;
              END IF;
          ELSE
              IF (tabela_var = 'funcionario') THEN
                  DELETE FROM funcionario WHERE cpf=campo6_var;
                  SET result = "funcionario - delete";
              ELSE 
                  IF (tabela_var = 'departamento') THEN
                      DELETE FROM departamento WHERE dnumero=campo9_var;
                      SET result = "departamento - delete";
                  ELSE
                      DELETE FROM depto_localizacoes WHERE dnumero=campo9_var;
                      SET result = "depto-localizacoes - delete";
                  END IF;
              END IF;
          END IF;
      END IF;
  END $$
DELIMITER ;




